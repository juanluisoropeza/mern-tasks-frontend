import React, { useContext, useState, useEffect } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareasContext';

const FormTarea = () => {

    // extraer si un proyecto esta activo
    const proyectosContext = useContext(proyectoContext);
    const { proyecto } = proyectosContext;

    // Obteber la funcion del context de tareas
    const tareasContext = useContext(tareaContext);
    const { 
        errorTarea, 
        tareaSeleccionada, 
        agregarTarea, 
        validarTarea, 
        obtenerTareas,
        modificarTarea,
        limpiarTarea
    } = tareasContext;

    // Effect que detecta si hay una tarea seleccionada
    useEffect( () => {
        if(tareaSeleccionada !== null) {
            guardarTarea(tareaSeleccionada);
        } else {
            guardarTarea({
                nombre: ''
            });
        }
    }, [tareaSeleccionada]);

    // state del formulario
    const [tarea, guardarTarea] = useState({
        nombre: '',
    });

    // extraer el nombre del proyecto
    const { nombre } = tarea;

    // Si no hay proyecto seleccionado
    if(!proyecto) return null;

    // Array destructuring para extraer el proyecto actual
    const [proyectoActual] = proyecto;

    // leer los valores del formulario

    const handleChange = (e) => {
        guardarTarea({
            ...tarea,
            [e.target.name]: e.target.value
        });
    };

    const onSubmit = (e) => {
        e.preventDefault();

        // validar
        if(nombre.trim() === '') {
            validarTarea();
            return;
        }

        // Revisar si es edicion o nueva tarea
        if(tareaSeleccionada === null) {
            // tarea nueva
            // agregar la nueva tarea al state de tareas
            tarea.proyectoId = proyectoActual.id;
            tarea.estado = false;
            agregarTarea(tarea);
        } else {
            // modificar la tarea existente
            modificarTarea(tarea);

            // Elimina tarea seleccionada del state
            limpiarTarea();
        }

        // obtener y filtrar las tareas del proyecto actual
        obtenerTareas(proyectoActual.id);

        // reiniciar el form
        guardarTarea({
            nombre: ''
        });

    };

    return (
        <div className="formulario">
            <form
                onSubmit={ onSubmit }
            >
                <div className="contenedor-input">
                    <input
                        type="text"
                        className="input-text"
                        placeholder="Nombre tarea..."
                        name="nombre"
                        onChange={ handleChange }
                        value={ nombre }
                    />
                </div>
                <div className="contenedor-input">
                    <button
                        type="submit"
                        className="btn btn-primario btn-submit btn-block"
                    >
                        { tareaSeleccionada ? 'Editar Tarea' : 'Agregar Tarea' }
                    </button>
                </div>
            </form>
            { errorTarea ? <p className="mensaje error">El nombre de la tarea es obligatorio.</p> : null }
        </div>
    );
}
 
export default FormTarea;

import React, { Fragment, useState, useContext } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';

const NuevoProyecto = () => {

    // Obtener el state del formulario
    const proyectosContext = useContext(proyectoContext);
    const { 
        formulario, 
        errorFormulario, 
        mostrarFormulario, 
        agregarProyecto, 
        mostrarError 
    } = proyectosContext;

    // state para proyecto 
    const [proyecto, guardarProyecto] = useState({
        nombre: '',
    });

    // extraer nombre de proyecto
    const { nombre } = proyecto;

    // lee los contenidos del input
    const onChangeProyecto = (e) => {
        guardarProyecto({
            ...proyecto,
            [e.target.name]: e.target.value
        });
    };

    // cuando el usuario envia un proyecto 
    const onSubmitProyecto = (e) => {
        e.preventDefault();

        // validar proyecto
        if(nombre === '') {
            mostrarError();
            return;
        }

        // agregar el state
        agregarProyecto(proyecto);

        // reiniciar el formulario
        guardarProyecto({
            nombre: '',
        });
    };

    // Mostrar el formulario
    const onClickNuevoProyecto = () => {
        mostrarFormulario();
    };

    return (
        <Fragment>
            <button
                type="button"
                className="btn btn-block btn-primario"
                onClick={ onClickNuevoProyecto  }
            >
                Nuevo Proyecto
            </button>
            {
                formulario 
                ?
                    (
                        <form
                            className="formulario-nuevo-proyecto"
                            onSubmit={ onSubmitProyecto }
                        >
                            <input
                                type="text"
                                className="input-text"
                                placeholder="Nombre Proyecto"
                                name="nombre"
                                onChange={ onChangeProyecto }
                                value={ nombre }
                            />
                            <button
                                type="submit"
                                className="btn btn-block btn-primario"
                            >
                                Agregar Proyecto
                            </button>
                        </form>
                    )
                : null
            }
            { errorFormulario ? <p className="mensaje error">El nombre del proyecto es obligatorio.</p> : null }
        </Fragment>
    );
}
 
export default NuevoProyecto;

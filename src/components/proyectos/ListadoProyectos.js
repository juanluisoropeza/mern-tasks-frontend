import React, { useContext, useEffect } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import Proyecto from './Proyecto';
import proyectoContext from '../../context/proyectos/proyectoContext';

const ListadoProyectos = () => {

    // extraer proyectos de state inicial
    const proyectosContext = useContext(proyectoContext);
    const { proyectos, obtenerProyectos } = proyectosContext;

    // obtener proyectos cuando carga el componente
    // (el useEffect jamas debe estar despues de un return)
    useEffect(() => {
        obtenerProyectos();

        // eslint-disable-next-line
    }, []);

    // revisar si proyectos tiene contenido
    if(proyectos.length === 0) return <p>No hay proyectos, comienza creando uno.</p>;

    return (
        <ul className="listado-proyectos">
            <TransitionGroup>
                { proyectos.map((proyecto) => (
                    <CSSTransition
                        key = { proyecto.id }
                        timeout = { 200 }
                        classNames="proyecto"
                    >
                        <Proyecto
                            proyecto={proyecto}
                        />
                    </CSSTransition>
                )) }
            </TransitionGroup>
        </ul>
    );
}
 
export default ListadoProyectos;
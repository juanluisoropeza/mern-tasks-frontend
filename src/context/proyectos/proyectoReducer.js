import { 
    FORMULARIO_PROYECTO, 
    OBTENER_PROYECTOS,
    AGREGAR_PROYECTO,
    VALIDAR_FORMULARIO,
    PROYECTO_ACTUAL,
    ELIMINAR_PROYECTO,
} from '../../types';

const ProyectoReducer = (state, action) => {
    switch (action.type) {
        case FORMULARIO_PROYECTO:
            return {
                ...state,
                formulario: true
            };
        case OBTENER_PROYECTOS:
            return {
                ...state,
                proyectos: action.payload
            };
        case AGREGAR_PROYECTO:
            return {
                ...state,
                proyectos: [action.payload, ...state.proyectos], // agrega los proyectos existentes en el state y ademas agrega el nuevo proyecto | si se invierte se agrega primero el proyecto y despues se agrega el listado
                formulario: false,
                errorFormulario: false,
            };
        case VALIDAR_FORMULARIO:
            return {
                ...state,
                errorFormulario: true
            };        
        case PROYECTO_ACTUAL:
            return {
                ...state,
                proyecto: state.proyectos.filter(proyecto => proyecto.id === action.payload)
            };
        
        case ELIMINAR_PROYECTO:
            return {
                ...state,
                proyectos: state.proyectos.filter(proyecto => proyecto.id !== action.payload),
                proyecto: null,
            };
        default:
            return state;
    }
};

export default ProyectoReducer;
import React, { useReducer } from 'react';
import { v4 as uuidv4 } from 'uuid';

import TareasContext from './tareasContext';
import TareasReducer from './tareasReducer';

import { 
    TAREAS_PROYECTO,
    AGREGAR_TAREA,
    VALIDAR_TAREA,
    ELIMINAR_TAREA,
    ESTADO_TAREA,
    TAREA_ACTUAL,
    ACTUALIZAR_TAREA,
    LIMPIAR_TAREA
} from '../../types';

const TareasState = (props) => {
    const initialState = {
        tareas: [
            { id: 1, nombre: 'Elegir Plataforma', estado: true, proyectoId: 1 },
            { id: 2, nombre: 'Elegir Colores', estado: false, proyectoId: 2 },
            { id: 3, nombre: 'Elegir Plataformas de Pago', estado: false, proyectoId: 3 },
            { id: 4, nombre: 'Elegir Hosting', estado: true, proyectoId: 4 },
            { id: 5, nombre: 'Elegir Plataforma', estado: true, proyectoId: 3 },
            { id: 6, nombre: 'Elegir Colores', estado: false, proyectoId: 1 },
            { id: 7, nombre: 'Elegir Plataformas de Pago', estado: false, proyectoId: 2 },
            { id: 8, nombre: 'Elegir Plataforma', estado: true, proyectoId: 4 },
            { id: 9, nombre: 'Elegir Colores', estado: false, proyectoId: 3 },
            { id: 10, nombre: 'Elegir Plataformas de Pago', estado: false, proyectoId: 4 },
            { id: 11, nombre: 'Elegir Plantilla', estado: false, proyectoId: 4 },
        ],
        tareasProyecto: null,
        errorTarea: false,
        tareaSeleccionada: null
    };

    // Crear dispatch y state
    const [state, dispatch] = useReducer(TareasReducer, initialState);

    // Crear las funciones

    // Obtener las tareas de un proyecto 
    const obtenerTareas = (proyectoId) => {
        dispatch({
            type: TAREAS_PROYECTO,
            payload: proyectoId
        });
    };

    // Agregar una tarea al proyecto seleccionado
    const agregarTarea = (tarea) => {        
        tarea.id = uuidv4();
        dispatch({
            type: AGREGAR_TAREA,
            payload: tarea
        });
    };

    // Valida y muestra un error en caso de que sea necesario
    const validarTarea = () => {
        dispatch({
            type: VALIDAR_TAREA
        });
    };

    // Eliminar tarea por id
    const eliminarTarea = (id) => {
        dispatch({
            type: ELIMINAR_TAREA,
            payload: id
        });
    };

    // Cambia el estado de cada tarea
    const cambiarEstadoTarea = (tarea) => {
        dispatch({
            type: ESTADO_TAREA,
            payload: tarea
        });
    };

    // Seleccionar la tarea actual | Extrae para edicion
    const guardarTareaActual = (tarea) => {
        dispatch({
            type: TAREA_ACTUAL,
            payload: tarea
        });
    };

    // Edita o modifica una tarea
    const modificarTarea = (tarea) => {
        dispatch({
            type: ACTUALIZAR_TAREA,
            payload: tarea
        });
    };

    // Elimina la tarea seleccionada (limpiar tarea despues de editarla)
    const limpiarTarea = () => {
        dispatch({
            type: LIMPIAR_TAREA
        });
    };

    return ( 
        <TareasContext.Provider
            value={{
                tareas: state.tareas,
                tareasProyecto: state.tareasProyecto,
                errorTarea: state.errorTarea,
                tareaSeleccionada: state.tareaSeleccionada,
                obtenerTareas,
                agregarTarea,
                validarTarea,
                eliminarTarea,
                cambiarEstadoTarea,
                guardarTareaActual,
                modificarTarea,
                limpiarTarea
            }}
        >
            { props.children }
        </TareasContext.Provider>
    );
}
 
export default TareasState;
